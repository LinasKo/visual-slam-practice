import os
import glob

import cv2
import numpy as np

from abstract_image_provider import AbstractImageProvider


class UQStLuciaDataProvider(AbstractImageProvider):
    """
    Opens and preprocesses the UQ St Lucia Dataset to be used for feature detection
    """

    IMG_WIDTH = 1024
    IMG_HEIGHT = 768

    DATA_DIR = os.path.join("data", "UQ St Lucia Stereo Vehicular Dataset", "whole", "101215_153851_MultiCamera0")

    CAMERA_PARAMETERS = [
        {
            "intrinsic_matrix": np.array([
                [1246.56167, 0, 532.28794],
                [0, 1247.09234, 383.60407],
                [0, 0, 1]
            ]),
            "distortion_coefficients": np.array([-0.32598, 0.18151, 0.00033, -0.00045, 0.00000]),
            "images_filename_pattern": "cam0_image[0-9][0-9][0-9][0-9][0-9].bmp",
        },
        {
            "intrinsic_matrix": np.array([
                [1244.80505, 0, 506.63554],
                [0, 1245.81569, 390.70481],
                [0, 0, 1]
            ]),
            "distortion_coefficients": np.array([-0.32512, 0.17124, 0.00003, 0.00003, 0.00000]),
            "images_filename_pattern": "cam1_image[0-9][0-9][0-9][0-9][0-9].bmp",
        }
    ]

    def __init__(self, camera_index, skip_first=0, skip_rate=0):
        """
        :param camera_index: int index, telling which camera is being used
        :param skip_first: Skip the first N images
        :param skip_rate: Skip N images before showing one
        """
        self.camera_index = camera_index

        assert skip_first >= 0
        self.skip_first = skip_first

        assert skip_rate >= 0
        self.skip_rate = skip_rate + 1
        print(f"Showing every {self.skip_rate}th frame")

        self._init_img_gen()

    def _init_img_gen(self):
        cam_params = self.CAMERA_PARAMETERS[self.camera_index]
        cam_pattern = os.path.join(self.DATA_DIR, cam_params["images_filename_pattern"])

        img_filenames = sorted(glob.glob(cam_pattern))
        self._img_fnames = []
        for i, fname in enumerate(img_filenames):
            if self.skip_first > 0:
                self.skip_first -= 1
                continue

            if i % self.skip_rate != 0:
                continue

            self._img_fnames.append(fname)

        print(f"Will show {len(self._img_fnames)} images in total")

    def gen_img(self):
        cam_params = self.CAMERA_PARAMETERS[self.camera_index]

        for fname in self._img_fnames:
            img = self._open_raw_image(fname)
            img = cv2.undistort(
                img,
                cam_params["intrinsic_matrix"],
                cam_params["distortion_coefficients"]
            )
            yield img

    @staticmethod
    def _open_raw_image(path):
        img = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
        img = cv2.cvtColor(img, cv2.COLOR_BAYER_GR2BGR)
        return img

    def get_intrinsic_matrix(self):
        return self.CAMERA_PARAMETERS[self.camera_index]["intrinsic_matrix"]

    def get_focal_length(self):
        intr_mtx = self.CAMERA_PARAMETERS[self.camera_index]["intrinsic_matrix"]
        focal_length = (intr_mtx[0, 0], intr_mtx[1, 1])
        return focal_length

    def get_principal_point(self):
        intr_mtx = self.CAMERA_PARAMETERS[self.camera_index]["intrinsic_matrix"]
        principal_point = (intr_mtx[0, 2], intr_mtx[1, 2])
        return principal_point
