import cv2
import numpy as np
import matplotlib.pyplot as plt
# noinspection PyUnresolvedReferences
from mpl_toolkits.mplot3d import Axes3D
from time import sleep


def draw_keypoints(orig_img, kp, color=(0, 255, 0)):
    img = orig_img.copy()
    for kp0 in kp:
        pt = kp0.pt
        cv2.circle(img, tuple(map(int, pt)), radius=1, color=color)
    return img


def draw_kp_matches(orig_img1, kp0, kp1, matches):
    img = orig_img1.copy()

    kp0_idx_matched = set()
    kp1_idx_matched = set()
    for dmatch in matches:
        """
        imgIdx -> Index of train image (always 0 here)
        queryIdx -> Index of descriptor (also keypoint) in image 0
        trainIdx -> Index of descriptor (also keypoint) in image 1
        """
        kp0_idx_matched.add(dmatch.queryIdx)
        kp1_idx_matched.add(dmatch.trainIdx)

    # Draw unmatched first to prevent occlusion
    for idx, k in enumerate(kp0):
        if idx not in kp0_idx_matched:
            cv2.circle(img, tuple(map(int, k.pt)), radius=1, color=[0, 0, 255])

    for idx, k in enumerate(kp1):
        if idx not in kp1_idx_matched:
            cv2.circle(img, tuple(map(int, k.pt)), radius=1, color=[0, 0, 255])

    # Draw matches next
    for dmatch in matches:
        pt0 = tuple(map(int, kp0[dmatch.queryIdx].pt))
        pt1 = tuple(map(int, kp1[dmatch.trainIdx].pt))

        cv2.circle(img, pt0, radius=1, color=[0, 255, 0])
        cv2.circle(img, pt1, radius=1, color=[0, 255, 0])

        cv2.line(img, pt0, pt1, color=[0, 255, 0])

    return img


def draw_pt_matches(orig_img1, pt0, pt1):
    assert len(pt0) == len(pt1)
    img = orig_img1.copy()

    for i in range(len(pt0)):
        pt0_0 = tuple(map(int, pt0[i]))
        pt1_0 = tuple(map(int, pt1[i]))

        cv2.circle(img, pt0_0, radius=1, color=[0, 255, 0])
        cv2.circle(img, pt1_0, radius=1, color=[0, 255, 0])

        cv2.line(img, pt0_0, pt1_0, color=[0, 255, 0])

    return img
