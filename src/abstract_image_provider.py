import abc


class AbstractImageProvider(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def gen_img(self):
        pass

    @abc.abstractmethod
    def get_intrinsic_matrix(self):
        pass

    @abc.abstractmethod
    def get_focal_length(self):
        pass

    @abc.abstractmethod
    def get_principal_point(self):
        pass
