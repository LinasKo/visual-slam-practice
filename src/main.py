import cv2
import numpy as np

from uq_st_lucia_image_provider import UQStLuciaDataProvider
from visual_odometry import VisualOdometry, DetectorType, DescriptorType
from bundle_adjustment import BundleAdjustment
from pangolin_plotter import PangolinPlotter


def show_multi_images(win_name, images, scale=1.0):
    h_images = np.hstack([
        cv2.resize(img, (int(len(img) * scale), int(len(img[0]) * scale))) for img in images
    ])
    cv2.namedWindow(win_name)
    cv2.imshow(win_name, h_images)


def show_single_image(win_name, img, scale=1.0):
    img = img.copy()
    img = cv2.resize(img, (int(len(img) * scale), int(len(img[0]) * scale)))
    cv2.namedWindow(win_name)
    cv2.imshow(win_name, img)


def compute_overlapping_point_indices(old_arr, new_arr, rounding_decimals=2):
    # Round in case there's noise in 3D point estimation (which there most likely is)
    old_arr = np.round(old_arr, decimals=rounding_decimals)
    new_arr = np.round(new_arr, decimals=rounding_decimals)

    # TODO: rounding can create duplicates

    # !!! Woah, if I pass in a single array as both old and new arrays, round it, and then intersect, the intersection
    # will find the values different, although visually they are the same. Interesting.

    overlapping_indices = np.in1d(new_arr[:, 0], old_arr[:, 0]) & \
                          np.in1d(new_arr[:, 1], old_arr[:, 1]) & \
                          np.in1d(new_arr[:, 2], old_arr[:, 2])

    return overlapping_indices


if __name__ == "__main__":
    plotter_3d = PangolinPlotter()
    try:
        plotter_3d.start()

        img_producer = UQStLuciaDataProvider(camera_index=0, skip_first=600, skip_rate=3)

        landmark_detector = VisualOdometry(
            DetectorType.GOOD_FEATURES,
            DescriptorType.ORB
        )

        intr_mtx = img_producer.get_intrinsic_matrix()
        pose_mtx = np.eye(4)
        plotter_3d.draw_3d_pose(pose_mtx)

        # Set camera for bundle adjuster
        focal_length = np.mean(img_producer.get_focal_length())
        principal_point = img_producer.get_principal_point()

        bundle_adjuster = BundleAdjustment()
        bundle_adjuster.set_cam_params(focal_length, principal_point)

        pose_ids = []
        for i, img in enumerate(img_producer.gen_img()):

            if i == 0:
                landmark_detector.initial_input(img)
                new_pose_id = next(landmark_detector.pose_id_generator)
                bundle_adjuster.add_pose(pose_mtx, new_pose_id, set_fixed=True)
                pose_ids.append(new_pose_id)

                prev_pts_3d = np.empty((0, 3))
                continue

            # Correct output of cv2.recoverPose found in https://answers.opencv.org/question/31421/opencv-3-essentialmatrix-and-recoverpose/?answer=183752#post-id-183752
            pts_r_mtx, pts_t_mtx, pts0, pts1, ids0, ids1, out_img = landmark_detector.odometry_step(img)
            cam_r_mtx = pts_r_mtx.T  # inv(R) = R.T, since it is orthogonal
            cam_t_mtx = -np.dot(pts_r_mtx.T, pts_t_mtx)  # inv(t)

            # Also see http://nghiaho.com/?p=2379 for just the formulas
            cam_rt_mtx = np.eye(4)
            cam_rt_mtx[:3, :3] = pts_r_mtx
            cam_rt_mtx[:3, 3:] = pts_t_mtx

            new_pose_mtx = np.dot(pose_mtx, cam_rt_mtx)

            np.set_printoptions(precision=2, suppress=True)
            print(new_pose_mtx)

            # Triangulate points

            # Option 1: Use relative transformation matrices
            proj_mtx0 = np.dot(intr_mtx, np.eye(3, 4))
            proj_mtx1 = np.dot(intr_mtx, cam_rt_mtx[0:3, 0:4])

            # Option 2: Use absolute transformation matrices
            # proj_mtx0 = np.dot(intr_mtx, pose_mtx[0:3, 0:4])
            # proj_mtx1 = np.dot(intr_mtx, new_pose_mtx[0:3, 0:4])
            # TODO: check if results are different for options 1 and 2. Though it seems equivalent right now

            # Note: throws "free(): corrupted unsorted chunks" if not converted to float
            # Takes points of shape (3, -1), returns (4, -1)
            pts_4d = cv2.triangulatePoints(proj_mtx0, proj_mtx1, pts0.T.astype(np.float), pts1.T.astype(np.float))
            pts_4d = pts_4d.T  # (4, -1) -> (-1, 4)

            # Point culling
            # TODO: see if they can be inverted instead of culled, as they seem to form a mirrored image behind the camera. Is the cheirality check failing?
            point_filter = pts_4d[:, 3] < 0
            pts_4d = pts_4d[point_filter]
            pts_3d = pts_4d[:, :3] / pts_4d[:, 3:]

            # # Apply current camera pose
            # pts_3d = pose_mtx[0:3, 3:4].T + np.dot(pts_3d, pose_mtx[0:3, 0:3])

            # # Attempt at rotating the matrix in an ad-hoc way in hopes of changing the resulting orientation
            # pangolin_pose_mtx = pose_mtx.copy()  # debug - try to rotate camera before plotting in 3D
            # pangolin_pose_mtx[0:3, 0:3] = np.dot(cv2.Rodrigues(np.array([np.pi / 2, np.pi / 2, np.pi / 2]).astype(float))[0], pangolin_pose_mtx[0:3, 0:3])
            # pts_3d = pangolin_pose_mtx[0:3, 3:4].T + np.dot(pts_3d, pangolin_pose_mtx[0:3, 0:3])

            # TODO: can return points that are not unique, even before rounding
            # TODO: what the hell did I mean by that? Are some points located at the same locations?

            pts_3d_colors = np.array([img[y][x] for x, y in pts1]) / 255.
            pts_3d_colors = pts_3d_colors[point_filter]
            pts_3d_colors[:, 0], pts_3d_colors[:, 2] = pts_3d_colors[:, 2], pts_3d_colors[:, 0].copy()  # BGR -> RGB

            # # Sort everything to make it easier to match IDs
            # # TODO: review if this is needed
            # pts_3d_sort_order = np.argsort(pts_3d[:, 0])
            # pts_3d_colors = pts_3d_colors[pts_3d_sort_order]
            # pts_3d = pts_3d[pts_3d_sort_order]
            # pts0 = pts0[pts_3d_sort_order]
            # pts1 = pts1[pts_3d_sort_order]
            # ids0 = ids0[pts_3d_sort_order]
            # ids1 = ids1[pts_3d_sort_order]

            # ### Bundle Adjust
            # overlapping_indices = compute_overlapping_point_indices(prev_pts_3d, pts_3d, rounding_decimals=0)
            #
            # old_pts0 = pts0[overlapping_indices]
            # old_pts1 = pts1[overlapping_indices]
            # old_pts_3d = pts_3d[overlapping_indices]
            # old_pts_ids0 = ids0[overlapping_indices]
            #
            # new_pts0 = pts0[~overlapping_indices]
            # new_pts1 = pts1[~overlapping_indices]
            # new_pts_3d = pts_3d[~overlapping_indices]
            # new_pts_ids1 = ids1[~overlapping_indices]
            #
            # assert len(np.unique(np.concatenate([old_pts_ids0, new_pts_ids1])) == len(old_pts_ids0) + len(new_pts_ids1))
            #
            # prev_pts_3d = pts_3d
            #
            # # Add new pose
            # prev_pose_id = pose_ids[-1]
            # new_pose_id = next(landmark_detector.pose_id_generator)
            # bundle_adjuster.add_pose(new_pose_mtx, new_pose_id, set_fixed=(i <= 1))
            # pose_ids.append(new_pose_id)
            #
            # # Add new points and edges
            # for new_pt_3d, new_pt1, new_pt_id1 in zip(new_pts_3d, new_pts1, new_pts_ids1):
            #     bundle_adjuster.add_point(new_pt_id1, new_pt_3d)
            #     bundle_adjuster.add_edge(new_pt_id1, new_pose_id, new_pt1)
            #
            # # Add edges to re-observed old points
            # for old_pt1, old_pt_id0 in zip(old_pts0, old_pts_ids0):
            #     bundle_adjuster.add_edge(old_pt_id0, new_pose_id, old_pt1)
            #
            # # Add pre-triangulation edges
            # if i == 1:
            #     assert len(ids1) == len(new_pts_ids1), "Expected all detected points to be new and have new IDs assigned"
            #     for new_pt0, new_pt_id1 in zip(new_pts0, new_pts_ids1):
            #         bundle_adjuster.add_edge(new_pt_id1, prev_pose_id, new_pt0)
            #
            # if i >= 10:
            #     # print("before:", bundle_adjuster.get_vertex_rotation(new_pose_id))
            #     # print("before:", bundle_adjuster.get_vertex_translation(new_pose_id))
            #     bundle_adjuster.optimize(max_iterations=10)
            #     # print("after:", bundle_adjuster.get_vertex_rotation(new_pose_id))
            #     # print("after:", bundle_adjuster.get_vertex_translation(new_pose_id))
            #
            #     new_pose_mtx = bundle_adjuster.get_vertex_pose(new_pose_id)

            # Display
            if out_img is not None:
                show_single_image("Matches", out_img, scale=0.8)

            plotter_3d.draw_3d_pose(new_pose_mtx)
            # plotter_3d.draw_3d_pose(pangolin_pose_mtx)
            plotter_3d.draw_3d_points(pts_3d, pts_3d_colors)

            pose_mtx = new_pose_mtx

            k = cv2.waitKey(0)
            if k == 27:  # Esc key
                break
    finally:
        plotter_3d.finish()
        print("Done.")
