import cv2
import numpy as np
from enum import Enum, auto
from itertools import count

from visualisation import draw_keypoints, draw_pt_matches, draw_kp_matches


class Point:
    def __init__(self, value, point_id):
        self.value = value
        self.point_id = point_id


class DetectorType(Enum):
    FAST = auto()
    ORB = auto()
    GOOD_FEATURES = auto()


class DescriptorType(Enum):
    BRISK = auto()
    ORB = auto()


class VisualOdometry:
    LOWES_DISTANCE_THRESHOLD = 40

    def __init__(self, detector_type, descriptor_type):
        self.detector_type = detector_type
        self.descriptor_type = descriptor_type

        # Cache
        self.prev_keypoints = None
        self.prev_descriptors = None
        self.prev_ids = None

        self.orb = None  # can be both detector and descriptor
        self._init_detector()
        self._init_descriptor()
        self._init_matcher()

        # ID generation
        self.point_id_generator = count(start=1, step=2)
        self.pose_id_generator = count(start=2, step=2)

    def _init_detector(self):
        if self.detector_type == DetectorType.FAST:
            # Features from Accelerated Segment Test
            self.fast = cv2.FastFeatureDetector_create()
        elif self.detector_type == DetectorType.ORB:
            # Oriented FAST and Rotated BRIEF
            self.orb = cv2.ORB_create(nfeatures=2000)
        elif self.detector_type == DetectorType.GOOD_FEATURES:
            # Corner detector function
            pass
        else:
            raise Exception(f"Unrecognized feature detector type '{self.detector_type}'")

    def _init_descriptor(self):
        if self.descriptor_type == DescriptorType.BRISK:
            # Binary Robust Invariant Scalable Keypoints
            self.brisk = cv2.BRISK_create()
        elif self.descriptor_type == DescriptorType.ORB:
            # Oriented FAST and Rotated BRIEF
            if not self.orb:
                self.orb = cv2.ORB_create(nfeatures=2000)
        else:
            raise Exception(f"Unrecognized feature descriptor type '{self.descriptor_type}'")

    def _init_matcher(self):
        # Brute-Force Matcher
        self.bf_matcher = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
        # crossCheck checks if features match both ways
        # crossCheck seems to prevent BFMatcher.knnMatch from running. If enabled, BFMatcher.match should be used.
        # crossCheck also seems to prevent ratio test from being used as resulting matches list is no longer iterable.  ???

    def initial_input(self, img):
        """
        Cache initial results for future use
        """
        assert self.prev_keypoints is None and self.prev_descriptors is None

        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        kp1, des1 = self.detect_features(gray)
        ids1 = self.create_point_ids(kp1)

        self.prev_keypoints = kp1
        self.prev_descriptors = des1
        self.prev_ids = ids1

    def odometry_step(self, img):
        assert self.prev_keypoints is not None and self.prev_descriptors is not None
        out_img = None

        kp0, des0, ids0 = self.prev_keypoints, self.prev_descriptors, self.prev_ids

        gray1 = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        kp1, des1 = self.detect_features(gray1)
        ids1 = self.create_point_ids(kp1)

        self.prev_keypoints = kp1
        self.prev_descriptors = des1
        self.prev_ids = ids1

        matches = self.match_features(des0, des1)
        # out_img = draw_kp_matches(img, kp0, kp1, matches)

        _, good_pts0, good_pts1, ids0, ids1 = self.lowes_test(kp0, kp1, matches, ids0, ids1)
        # out_img = draw_pt_matches(img, good_pts0, good_pts1)

        pts0, pts1 = np.int32(good_pts0), np.int32(good_pts1)

        e_mtx, mask = cv2.findEssentialMat(pts0, pts1)
        # Uses RANSAC by default. RANSAC and LMEDS give mask for outlier detection

        # Remove outliers
        inlier_indices = mask[:, 0] == 1
        pts0 = pts0[inlier_indices]
        pts1 = pts1[inlier_indices]

        ids0 = np.array(ids0).reshape(-1, 1)[inlier_indices]
        ids1 = np.array(ids1).reshape(-1, 1)[inlier_indices]
        ids0 = ids0.squeeze()
        ids1 = ids1.squeeze()

        # Returns rotation and translation matrix OF THE POINTS
        # That is: pts2 = np.dot(r_mtx, pts0) + t_mtx
        # More: https://answers.opencv.org/question/31421/opencv-3-essentialmatrix-and-recoverpose/
        retval, pts_r_mtx, pts_t_mtx, mask = cv2.recoverPose(e_mtx, pts0, pts1)

        out_img = draw_pt_matches(img, pts0, pts1)
        return pts_r_mtx, pts_t_mtx, pts0, pts1, ids0, ids1, out_img

    def detect_features(self, img):
        assert len(img.shape) == 2

        if self.detector_type == DetectorType.FAST:
            kp = self.fast.detect(img)
        elif self.detector_type == DetectorType.ORB:
            kp = self.orb.detect(img, None)
        elif self.detector_type == DetectorType.GOOD_FEATURES:
            corners = cv2.goodFeaturesToTrack(
                img,
                maxCorners=3000,
                qualityLevel=0.01,
                minDistance=3,
                blockSize=3,  # default: 3
                useHarrisDetector=False,  # Default: False
                k=0.04  # Default: 0.04
            )
            kp = [cv2.KeyPoint(corner[0], corner[1], 0) for corner in np.squeeze(corners)]
        else:
            raise Exception("Unknown feature detector type")

        if self.descriptor_type == DescriptorType.BRISK:
            kp, des = self.brisk.compute(img, kp)
        elif self.descriptor_type == DescriptorType.ORB:
            kp, des = self.orb.compute(img, kp)
        else:
            raise Exception("Unknown feature descriptor type")

        return kp, des

    def match_features(self, des0, des1):
        matches = self.bf_matcher.match(des0, des1)
        return matches

    def lowes_test(self, kp0, kp1, matches, ids0, ids1):
        # Remove outliers - Lowe's method
        assert len(kp0) == len(ids0) and len(kp1) == len(ids1), f"Lengths: {len(kp0)} == {len(ids0)} and {len(kp1)} == {len(ids1)}"

        good_matches = []
        good_pts0 = []
        good_pts1 = []
        good_ids0 = []
        good_ids1 = []
        for m in matches:
            """
            imgIdx -> Index of train image (always 0 here)
            queryIdx -> Index of descriptor (also keypoint) in image 0
            trainIdx -> Index of descriptor (also keypoint) in image 1
            """
            if m.distance < self.LOWES_DISTANCE_THRESHOLD:
                good_matches.append(m)
                good_pts0.append(kp0[m.queryIdx].pt)
                good_pts1.append(kp1[m.trainIdx].pt)
                good_ids0.append(ids0[m.queryIdx])
                good_ids1.append(ids1[m.trainIdx])

        return good_matches, good_pts0, good_pts1, good_ids0, good_ids1

    def create_point_ids(self, iterable):
        """
        Create a unique point ID for every given point
        :param iterable: any iterable, containing points of some sort, that should obey the point ID schema
        """
        return [next(self.point_id_generator) for _ in iterable]
