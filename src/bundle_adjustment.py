import numpy as np
import g2o


class BundleAdjustment(g2o.SparseOptimizer):
    def __init__(self, ):
        super().__init__()
        solver = g2o.BlockSolverSE3(g2o.LinearSolverCholmodSE3())
        solver = g2o.OptimizationAlgorithmLevenberg(solver)
        super().set_algorithm(solver)

    def set_cam_params(self, focal_length, principal_point):
        """
        :param focal_length: float
        :param principal_point: tuple(float, float)
        """
        cam = g2o.CameraParameters(focal_length, principal_point, 0)
        cam.set_id(0)
        super().add_parameter(cam)

    def add_pose(self, pose_mtx, pose_id, set_fixed=False):
        pose = g2o.SE3Quat(pose_mtx[:3, :3], pose_mtx[:3, 3])
        v_se3 = g2o.VertexSE3Expmap()
        v_se3.set_id(pose_id)
        v_se3.set_estimate(pose)
        v_se3.set_fixed(set_fixed)
        super().add_vertex(v_se3)

    def add_point(self, point_id, point, marginalized=True):
        v_p = g2o.VertexSBAPointXYZ()
        v_p.set_id(point_id)
        v_p.set_marginalized(marginalized)
        v_p.set_estimate(point)
        v_p.set_fixed(False)
        super().add_vertex(v_p)

    def add_edge(self, point_id, pose_id, measurement, robust_kernel=False):   # 95% CI
        edge = g2o.EdgeProjectXYZ2UV()
        edge.set_vertex(0, self.vertex(point_id))
        edge.set_vertex(1, self.vertex(pose_id))
        edge.set_measurement(measurement.astype(np.float64))   # projection
        edge.set_information(np.identity(2))
        if robust_kernel:
            edge.set_robust_kernel(g2o.RobustKernelHuber(np.sqrt(5.991)))
        edge.set_parameter_id(0, 0)
        super().add_edge(edge)

    def optimize(self, max_iterations=10):
        super().initialize_optimization()
        # super().set_verbose(True)
        super().optimize(max_iterations)

    def get_vertex_rotation(self, vertex_id):
        return self.vertex(vertex_id).estimate().rotation().rotation_matrix()

    def get_vertex_translation(self, vertex_id):
        return self.vertex(vertex_id).estimate().translation().reshape(3, 1)

    def get_vertex_pose(self, vertex_id):
        return self.vertex(vertex_id).estimate().matrix()
